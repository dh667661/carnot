import psutil

cpu_percent = psutil.cpu_percent()
mem = psutil.virtual_memory()
print(f'CPU usage: {cpu_percent}%, Memory usage: {mem.percent}%')
