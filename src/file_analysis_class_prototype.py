class FileAnalysis:
    def __init__(self):
        self.file_count = 0
        self.empty_dirs = 0
        self.empty_files = 0
        self.user_created_files = 0
        self.program_created_files = 0
        self.files_info = {}

    def process_file(self, file):
        """
        Perform some operation on the file, for example, add it to a data structure for fast lookups.
        """
        self.file_count += 1
        if os.path.getsize(file) == 0:
            self.empty_files += 1
            self.files_info[file] = {"type": "empty"}
        else:
            file_metadata = os.stat(file)
            if file_metadata.st_uid == os.getuid(): # check if the file is created by the current user
                self.user_created_files += 1
                self.files_info[file] = {"type": "user_created"}
            else:
                self.program_created_files += 1
                self.files_info[file] = {"type": "program_created"}

    def process_directory(self, directory):
        """
        Traverse the directory structure and add all files to the queue.
        """
        for root, dirs, files in os.walk(directory):
            if not files:
                self.empty_dirs += 1
                self.files_info[root] = {"type": "empty_directory"}
            for file in files:
                self.process_file(os.path.join(root, file))
