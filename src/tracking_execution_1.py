import time

def main():
    """
    Create a queue and start multiple worker threads to process the files.
    """
    q = Queue()
    for i in range(num_threads):
        t = Thread(target=worker, args=(q,))
        t.daemon = True
        t.start()
    
    start_time = time.time()
    process_directory(directory, q)
    q.join()
    end_time = time.time()
    total_time = end_time - start_time

    total_files = q.qsize()
    processing_rate = total_files / total_time

    print("Total files processed: ", total_files)
    print("Total time taken: ", total_time, "seconds")
    print("Processing rate: ", processing_rate, "files/second")

if __name__ == '__main__':
    main()
