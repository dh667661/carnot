def process_directory(directory, q):
    """
    Traverse the directory structure and add all files to the queue.
    """
    empty_dirs = 0
    for root, dirs, files in os.walk(directory):
        if not files:
            empty_dirs += 1
        for file in files:
            q.put(os.path.join(root, file))
    return empty_dirs

def main():
    """
    Create a queue and start multiple worker threads to process the files.
    """
    q = Queue()
    for i in range(num_threads):
        t = Thread(target=worker, args=(q,))
        t.daemon = True
        t.start()
    
    start_time = time.time()
    empty_dirs = process_directory(directory, q)
    q.join()
    end_time = time.time()
    total_time = end_time - start_time

    total_files = q.qsize()
    processing_rate = total_files / total_time

    print("Total files processed: ", total_files)
    print("Total Empty Directories found: ", empty_dirs)
    print("Total time taken: ", total_time, "seconds")
    print("Processing rate: ", processing_rate, "files/second")

if __name__ == '__main__':
    main()
