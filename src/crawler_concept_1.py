import os
from threading import Thread
from queue import Queue

def process_directory(directory, q):
    """
    Traverse the directory structure and add all files to the queue.
    """
    for root, dirs, files in os.walk(directory):
        for file in files:
            q.put(os.path.join(root, file))

def worker(q):
    """
    Process the files from the queue, for example by adding them to a data structure for fast lookups.
    """
    while True:
        file = q.get()
        # do something with the file
        q.task_done()

def main():
    """
    Create a queue and start multiple worker threads to process the files.
    """
    q = Queue()
    for i in range(num_threads):
        t = Thread(target=worker, args=(q,))
        t.daemon = True
        t.start()
    process_directory(directory, q)
    q.join()

if __name__ == '__main__':
    main()
